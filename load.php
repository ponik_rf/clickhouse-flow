<?php

include("./vendor/autoload.php");

$config = [
    'host' => '192.168.88.242',
    'port' => '8123',
    'username' => 'default',
    'password' => 'lehfktq',
];

$db = new ClickHouseDB\Client($config);
$db->database('default');
$db->setTimeout(1.5);      // 1500 ms
$db->setTimeout(10);       // 10 seconds
$db->setConnectTimeOut(5); // 5 seconds

function getLineArray($line)
{
    return preg_split('/([ ]+)/', $line);
}

function getTime($row){
    $s = explode('.',$row[1]);
    if (count($s) == 2) return $s[0];
    return $row[1];
}

$rows = [];
while (1) {
    $line = trim(fgets(STDIN));
    if (strlen($line) == 0) break;
    $lineArray = getLineArray($line);
    $Time = getTime($lineArray);
    $rows[] = [
        $lineArray[0].' '.$Time, // DATETIME
        $lineArray[2],      // SRC IP
        $lineArray[3],      // DST IP
        (int)$lineArray[4],      // SRC PORT
        (int)$lineArray[5],      // DST PORT
        (int)$lineArray[6],      // Bytes
        (int)$lineArray[7],      // BPS
    ];
}

$db->insert('flow',$rows,['times','src_ip','dst_ip','src_port','dst_port','bytes','bps']);

?>